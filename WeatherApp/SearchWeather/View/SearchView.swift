//
//  MainView.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit
import SnapKit

enum SearchViewState {
    case center
    case top
}

class SearchView: BaseView {
    var searchLocation = UITextField()
    var userLocationButton = UIButton()
    var searchButton = UIButton()
    var tableViewAnsweres = UITableView()
    private var container = UIView()

    override init() {
        super.init()
        self.backgroundColor = .white
        
        container.backgroundColor = .clear
        addSubview(container)
        
        searchLocation.backgroundColor = .clear
        searchLocation.textColor = .black
        searchLocation.layer.borderColor = UIColor.black.cgColor
        searchLocation.layer.borderWidth = 1
        searchLocation.placeholder = "Wprowadź miasto"
        container.addSubview(searchLocation)
        
        userLocationButton.backgroundColor = .blue
        userLocationButton.setTitle("Lokalizacja", for: .normal)
        userLocationButton.setTitleColor(.white, for: .normal)
        container.addSubview(userLocationButton)
        
        searchButton.backgroundColor = .blue
        searchButton.setTitle("Szukaj", for: .normal)
        searchButton.setTitleColor(.white, for: .normal)
        container.addSubview(searchButton)
        
        tableViewAnsweres.backgroundColor = .clear
        tableViewAnsweres.separatorStyle = .none
        addSubview(tableViewAnsweres)
        
        createConstraints(viewState: .center)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraintsForDefault() {
        createConstraints(viewState: .top)
    }
    
    func hideLocationButton() {
        userLocationButton.isHidden = true
    }
    
    func showLocationButton() {
        userLocationButton.isHidden = false
    }
    
    private func createConstraints(viewState: SearchViewState) {
        container.snp.remakeConstraints { (make) in
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            
            switch viewState {
            case .center:
                let offset = (UIScreen.main.bounds.size.height / 2) - (50 * 2)
                make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(offset)
            case .top:
                make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10)
            }
        }
        
        searchLocation.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(container)
            make.height.equalTo(50)
        }
        
        searchButton.snp.makeConstraints { (make) in
            make.top.equalTo(searchLocation.snp.bottom).offset(15)
            make.left.right.equalTo(container)
            make.height.equalTo(50)
        }
        
        userLocationButton.snp.makeConstraints { (make) in
            make.top.equalTo(searchButton.snp.bottom).offset(15)
            make.left.right.equalTo(container)
            make.bottom.equalTo(container)
            make.height.equalTo(50)
        }
        
        tableViewAnsweres.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self)
            make.top.equalTo(container.snp.bottom).offset(20)
        }
    }
}
