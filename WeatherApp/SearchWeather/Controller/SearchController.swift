//
//  MainController.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class SearchController: BaseViewController {
    private var searchView: SearchView?
    private var location = CLLocationManager()
    private var cities: Cities?
    private var weather: Weather?
    
    override func loadView() {
        super.loadView()
        searchView = SearchView()
        view = searchView
        searchView?.tableViewAnsweres.delegate = self
        searchView?.tableViewAnsweres.dataSource = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Weather"
        setupLocalization()

        searchView?.searchButton.addTarget(self,
                                         action: #selector(searchButtonAction),
                                         for: .touchUpInside)
        searchView?.userLocationButton.addTarget(self,
                                               action: #selector(currentLocationButtonAction),
                                               for: .touchUpInside)
    }
    
    //MARK:- Buttons Actions
    @objc
    private func currentLocationButtonAction() {
        location.startUpdatingLocation()
        startSpinner()
    }
    
    @objc
    private func searchButtonAction() {
        searchView?.setupConstraintsForDefault()
        startSpinner()
        searchLocation()
    }
    
    //MARK:- Localization
    
    private func setupLocalization() {
        location.delegate = self
        location.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            location.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        handleLocalization(status: CLLocationManager.authorizationStatus())
    }
    
    private func searchLocation() {
        guard let locationText = searchView?.searchLocation.text, !locationText.isEmpty else {
            showLocationAlert()
            stopSpinner()
            return
        }
        
        ApiRequest.shared.searchLoaction(name: locationText) { [weak self] (cities) in
            guard let cities = cities else { return }
            self?.cities = cities
            DispatchQueue.main.async {
                self?.searchView?.tableViewAnsweres.reloadData()
                self?.stopSpinner()
            }
        }
    }
    
    private func getLocationDetails(city: City) {
        ApiRequest.shared.getWeatherDetails(woeid: city.woeid) { [weak self] (weather) in
            guard let weather = weather else { return }
            self?.weather = weather
            DispatchQueue.main.async {
                self?.navigationController?.pushViewController(WeatherDetailsController(weather: weather,
                                                                                        cityName: city.title),
                                                               animated: true)
                self?.stopSpinner()
            }
        }
    }
    
    private func showLocationAlert() {
        let locationAlert = UIAlertController(title: "Nieprawidłowa lokalizacja",
                                              message: "Wprowadź prawidłowe dane",
                                              preferredStyle: .alert)
        
        locationAlert.addAction(UIAlertAction(title: "OK",
                                              style: .default,
                                              handler: nil))
        self.present(locationAlert, animated: true)
    }
    
    private func handleLocalization(status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            searchView?.showLocationButton()
        case .authorizedWhenInUse:
            searchView?.showLocationButton()
        case .denied:
            searchView?.hideLocationButton()
        case .notDetermined:
            searchView?.hideLocationButton()
        case .restricted:
            searchView?.hideLocationButton()
        default:
            searchView?.hideLocationButton()
        }
    }
}

//MARK:- TableViewDelegate
extension SearchController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        let city = cities?[indexPath.row]
        cell.textLabel?.text = city?.title
        cell.textLabel?.textColor = .black
        cell.backgroundColor? = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let city = cities?[indexPath.row] {
            startSpinner()
            getLocationDetails(city: city)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

//MARK:- LocationDelegate
extension SearchController: CLLocationManagerDelegate {
    func locationManager(_ location: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let loc = locations.first {
            print("\(loc)")
        
            ApiRequest.shared.findLoaction(latt: loc.coordinate.latitude.description,
                                           long: loc.coordinate.longitude.description) { [weak self] (cities) in
                guard let cities = cities, let city = cities.first else { return }
                self?.getLocationDetails(city: city)
                location.stopUpdatingLocation()
                self?.stopSpinner()
            }
        }
    }

    func locationManager(_ location: CLLocationManager, didFailWithError error: Error) {
        print("Nie można znaleźć lokalizacji: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        handleLocalization(status: status)
    }
}
