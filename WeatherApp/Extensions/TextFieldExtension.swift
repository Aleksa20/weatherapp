//
//  TextFieldExtension.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    var isEmpty: Bool {
        if let text = text, !text.isEmpty {
            return false
        }
        return true
    }
}
