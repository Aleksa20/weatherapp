//
//  ApiRequest.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import Foundation

class ApiRequest {
    static var shared = ApiRequest()
    
    func searchLoaction(name: String, completion: @escaping (_ cities: Cities?)->()) {
        guard let url = URL(string: "https://www.metaweather.com/api/location/search/?query=\(name)") else { return }
        
        makeRequest(model: Cities.self, url: url) { (cities) in
            completion(cities)
        }
    }
    
    func findLoaction(latt: String, long: String, completion: @escaping (_ cities: Cities?)->()) {
        guard let url = URL(string: "https://www.metaweather.com/api/location/search/?lattlong=\(latt),\(long)") else { return }
        
        makeRequest(model: Cities.self, url: url) { (cities) in
            completion(cities)
        }
    }
    
    func getWeatherDetails(woeid: Int, completion: @escaping (_ detailsWeather: Weather?)->()) {
        guard let url = URL(string: "https://www.metaweather.com/api/location/\(woeid)")
            else { return }
        
        makeRequest(model: Weather.self, url: url) { (weather) in
            completion(weather)
        }
        
    }
    
    //MARK:- Private Methods
    private func makeRequest<T: Codable>(model: T.Type, url: URL, completion: @escaping (_ model: T?)->()) {
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request) { (data, respone, error) in
            if error != nil {
                completion(nil)
                print("Request error: \(String(describing: error))")
                return
            }
            
            do {
                let weatherDetails = try JSONDecoder().decode(T.self, from: data!)
                print(weatherDetails)
                completion(weatherDetails)
            } catch {
                print("Error during JSON serialization: \(String(describing: error))")
            }
        }
        task.resume()
    }
}
