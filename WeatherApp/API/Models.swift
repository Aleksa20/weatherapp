//
//  Models.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Weather
struct Weather: Codable {
    let consolidatedWeather: [ConsolidatedWeather]
    let time, sunRise, sunSet, timezoneName: String
    let parent: Parent
    let sources: [Source]
    let title, locationType: String
    let woeid: Int
    let lattLong, timezone: String

    enum CodingKeys: String, CodingKey {
        case consolidatedWeather = "consolidated_weather"
        case time
        case sunRise = "sun_rise"
        case sunSet = "sun_set"
        case timezoneName = "timezone_name"
        case parent, sources, title
        case locationType = "location_type"
        case woeid
        case lattLong = "latt_long"
        case timezone
    }
}

// MARK: - ConsolidatedWeather
enum WeatherState: String, Codable {
    case snow = "sn"
    case sleet = "sl"
    case hail = "h"
    case thunderstorm = "t"
    case heavyRain = "hr"
    case lightRain = "lr"
    case showers = "s"
    case heavyCloud = "hc"
    case lightCloud = "lc"
    case clear = "c"
    
    func getImageURL() -> URL? {
        if let url = URL(string: "https://www.metaweather.com/static/img/weather/png/64/\(self.rawValue).png") {
            return url
        }
        return nil
    }
}

struct ConsolidatedWeather: Codable {
    let id: Int
    let weatherStateName, windDirectionCompass, created: String
    let weatherStateAbbr: WeatherState
    let applicableDate: String
    let minTemp, maxTemp, theTemp, windSpeed: Double
    let windDirection, airPressure: Double
    let humidity: Int
    let visibility: Double
    let predictability: Int

    enum CodingKeys: String, CodingKey {
        case id
        case weatherStateName = "weather_state_name"
        case weatherStateAbbr = "weather_state_abbr"
        case windDirectionCompass = "wind_direction_compass"
        case created
        case applicableDate = "applicable_date"
        case minTemp = "min_temp"
        case maxTemp = "max_temp"
        case theTemp = "the_temp"
        case windSpeed = "wind_speed"
        case windDirection = "wind_direction"
        case airPressure = "air_pressure"
        case humidity, visibility, predictability
    }
}

// MARK: - Parent
struct Parent: Codable {
    let title, locationType: String
    let woeid: Int
    let lattLong: String

    enum CodingKeys: String, CodingKey {
        case title
        case locationType = "location_type"
        case woeid
        case lattLong = "latt_long"
    }
}

// MARK: - Source
struct Source: Codable {
    let title, slug: String
    let url: String
    let crawlRate: Int

    enum CodingKeys: String, CodingKey {
        case title, slug, url
        case crawlRate = "crawl_rate"
    }
}

// MARK: - City
struct City: Codable {
    let title, locationType: String
    let woeid: Int
    let lattLong: String

    enum CodingKeys: String, CodingKey {
        case title
        case locationType = "location_type"
        case woeid
        case lattLong = "latt_long"
    }
}

typealias Cities = [City]
