//
//  WeatherDetailsController.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit
import SDWebImage

class WeatherDetailsController: BaseViewController {
    var weatherDetailsView: WeatherDetailsView?
    let weather: Weather
    let cityName: String
    
    init(weather: Weather, cityName: String) {
        self.weather = weather
        self.cityName = cityName
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        weatherDetailsView = WeatherDetailsView()
        view = weatherDetailsView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = cityName
        weatherDetailsView?.tableViewResults.delegate = self
        weatherDetailsView?.tableViewResults.dataSource = self
        weatherDetailsView?.tableViewResults.register(WeatherDetailsCell.self, forCellReuseIdentifier: WeatherDetailsCell.identifier)
    }
}

extension WeatherDetailsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather.consolidatedWeather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherDetailsCell.identifier, for: indexPath) as! WeatherDetailsCell
        let consWeather = weather.consolidatedWeather[indexPath.row]
        cell.datelabel.text = consWeather.applicableDate
        cell.weatherLabel.text = consWeather.weatherStateName
        cell.temperatureLabel.text = "Temperatura: \(String(format: "%.0f", consWeather.theTemp))°C"
        cell.airPressuerLabel.text = "Ciśnienie: \(String(format: "%.0f", consWeather.airPressure)) hPa"
        cell.weatherImage.sd_setImage(with: consWeather.weatherStateAbbr.getImageURL(), completed: nil)
        cell.textLabel?.textColor = .black
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}
