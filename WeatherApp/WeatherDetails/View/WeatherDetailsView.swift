//
//  WeatherDetailsView.swift
//  WeatherApp
//
//  Created by Alexa on 10/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit
import SnapKit

class WeatherDetailsView: BaseView {
    var tableViewResults = UITableView()
    
    override init() {
        super.init()

        addSubview(tableViewResults)
        tableViewResults.backgroundColor = .clear
        tableViewResults.separatorStyle = .none
        
        createConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createConstraints() {
        tableViewResults.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self)
        }
    }
}
