//
//  WeatherDetailsCell.swift
//  WeatherApp
//
//  Created by Alexa on 11/07/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit
import SnapKit

class WeatherDetailsCell: UITableViewCell {
    static let identifier = "cell"
    let defaultMargin: CGFloat = 10.0
    
    var container = UIView()
    var weatherLabel = UILabel()
    var datelabel = UILabel()
    var weatherImage = UIImageView()
    var temperatureLabel = UILabel()
    var airPressuerLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        container.translatesAutoresizingMaskIntoConstraints = false
        container.layer.borderColor = UIColor.black.cgColor
        container.layer.borderWidth = 1
        container.layer.cornerRadius = 5
        container.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.clipsToBounds = true
        self.addSubview(container)
        
        container.addSubview(weatherLabel)
        container.addSubview(datelabel)
        container.addSubview(temperatureLabel)
        container.addSubview(airPressuerLabel)
        weatherImage.contentMode = .scaleAspectFit
        container.addSubview(weatherImage)
        
        setupTextColor()
        createConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTextColor() {
        [weatherLabel, datelabel, temperatureLabel, airPressuerLabel].forEach({$0.textColor = .white})
    }
    
    private func configureDate() {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE, MMM d, yyyy"
        
        if let date = dateFormatterGet.date(from: "yyyy-MM-dd") {
            print(dateFormatterPrint.string(from: date))
        }
            
    }
    
    private func createConstraints() {
        container.snp.makeConstraints { (make) in
            make.top.left.equalTo(self).offset(defaultMargin)
            make.right.bottom.equalTo(self).offset(-defaultMargin)
        }

        datelabel.snp.makeConstraints { (make) in
            make.right.equalTo(container)
            make.left.top.equalTo(defaultMargin)
        }
        
        weatherLabel.snp.makeConstraints { (make) in
            make.top.equalTo(datelabel.snp.bottom).offset(5)
            make.right.equalTo(container)
            make.left.equalTo(defaultMargin)
        }
        
        temperatureLabel.snp.makeConstraints { (make) in
            make.top.equalTo(weatherLabel.snp.bottom).offset(5)
            make.right.equalTo(container)
            make.left.equalTo(defaultMargin)
        }
        
        airPressuerLabel.snp.makeConstraints { (make) in
            make.top.equalTo(temperatureLabel.snp.bottom).offset(5)
            make.right.equalTo(container)
            make.left.equalTo(defaultMargin)
        }
        
        weatherImage.snp.makeConstraints { (make) in
            make.top.equalTo(container)
            make.bottom.equalTo(container)
            make.right.equalTo(-defaultMargin)
            make.width.equalTo(100)
        }
    }
}
