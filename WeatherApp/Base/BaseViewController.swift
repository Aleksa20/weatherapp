//
//  BaseViewController.swift
//  WeatherApp
//
//  Created by Alexa on 08/09/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    private let spinner = UIActivityIndicatorView(style: .large)
    private var spinnerBackground = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinnerBackground.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(spinnerBackground)
        
        spinner.color = .white
        spinnerBackground.addSubview(spinner)
        spinnerBackground.isHidden = true
        
        setupConstraints()
    }
    
    func startSpinner() {
        DispatchQueue.main.async { [weak self] in
            self?.spinnerBackground.isHidden = false
            self?.spinner.startAnimating()
        }
    }
    
    func stopSpinner() {
        DispatchQueue.main.async { [weak self] in
            self?.spinnerBackground.isHidden = true
            self?.spinner.stopAnimating()
        }
    }
    
    private func setupConstraints() {
        spinnerBackground.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(self.view)
        }
        
        spinner.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view)
            make.centerY.equalTo(self.view)
        }
    }
}
