//
//  BaseView.swift
//  WeatherApp
//
//  Created by Alexa on 08/09/2020.
//  Copyright © 2020 Alexa. All rights reserved.
//

import UIKit

class BaseView: UIView {
    private var backgroundImage = UIImageView()
    
    init() {
        super.init(frame: .zero)
        backgroundImage.image = #imageLiteral(resourceName: "weatherBackground")
        addSubview(backgroundImage)
        backgroundImage.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(self)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
